/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calc;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Victor
 */
public class Calculadora_InterfazTest {
    
    public Calculadora_InterfazTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Calculadora_Interfaz.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        Calculadora_Interfaz.main(args);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    @Test
    public void testDivicion(){
        System.out.println("Prueba de la division");
        //Arrange
        Operaciones instance=new Operaciones();
        float expResult = 2.0F;
        //Act
        float result=instance.botonDivision(4, 2);
        //Assert
        assertEquals(Math.round(expResult), Math.round(result), 0.0);
    }
    
    @Test
    // Juan_MESSI10
    public void testResta(){
        System.out.println("** PRUEBA DE LA RESTA **");
        //Arrange
        Operaciones instance = new Operaciones();
        float expResult = 5.0F;
        //Act
        float result = instance.botonResta(20, 15);
        //Assert
        assertEquals(Math.round(expResult), Math.round(result), 0.0);
    }
    @Test
    public void TestMultiplicacion(){
        System.out.println("Prueba multiplicacion");
        //Arrange
        Operaciones instance = new Operaciones();
        float expResult = 18.0F;
        //Act
        float result = instance.botonMultiplicacion(9, 2);
        //Assert
        assertEquals(Math.round(expResult), Math.round(result),0.0);
    }
    
    @Test
    public void TestSuma(){
        System.out.println("Tesr de la suma");
        //Arrange
        Operaciones instance = new Operaciones();
        float expResult = 10.0F;
        //Act
        float result = instance.botonSuma(5, 5);
        //Assert
        assertEquals(Math.round(expResult), Math.round(result),0.0);
    }
}
